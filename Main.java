import java.util.Scanner;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args){
        Pizza[] wishlist = new Pizza[100];
        int orderNr = 1;

        while (true) {
            System.out.println("Welcome! To order a Pizza, just type its name. To view your current order, type \\\"?\\\"\"");

            Scanner sc = new Scanner(System.in);
            String input = sc.nextLine();

            if (input.equals("?")) {
                for (int i = 0; i < wishlist.length; i++) {
                    if (wishlist[i] == null) continue;
                    System.out.printf("%s: %s%n", i, wishlist[i].getName());
                }
                System.out.println("Enter a Number to delete the corresponding Pizza");

                String input2 = sc.nextLine();
                int eingabeInt = 0;

                try {
                    eingabeInt = Integer.parseInt(input2);
                } catch(NumberFormatException nfe) {
                    if (input2.equals("X")) {
                        System.out.println("Goodbye!");
                        break;
                    }
                    System.out.println("Please enter a valid number or X to close this view.");
                    continue;
                }

                if (wishlist[eingabeInt].getName() == null) {
                    System.out.println("This item isn't on your list.");
                    continue;
                }

                wishlist[Integer.parseInt(input2)] = null;
            } else {
                System.out.println("Select a size of small, medium, large");
                String groesse = sc.nextLine();

                Pizza neueBestellung = new Pizza(input, groesse);
                if (neueBestellung.getName() == null || neueBestellung.getPrice() == 0) continue;
                wishlist[orderNr-1] = neueBestellung;

                System.out.printf("The Pizza's price is %s%n", wishlist[orderNr-1].getPrice());
                orderNr++;
            }
        }
    }
}