import java.util.ArrayList;
import java.util.List;

public class Pizza {
    private String name, size;
    private List<String> toppings = new ArrayList<String>();
    private int basePrice;

    public Pizza(String order, String size) {
        switch (order) {
            case "Margherita" -> {
                name = order;
                toppings.add("Tomato");
                basePrice = 580;
                this.size = size;
            }
            case "Hawaii" -> {
                name = order;
                toppings.add("Pinapple");
                toppings.add("Jam");
                basePrice = 600;
                this.size = size;
            }
            case "Frutti di Mare" -> {
                name = order;
                toppings.add("Muscles");
                basePrice = 650;
                this.size = size;
            }
            default -> {
                System.out.printf("There's no Pizza %s", order);
            }
        }
    }

    public double getPrice() {
        double sizeSurcharge = 1;

        switch (size) {
            case "small" -> sizeSurcharge = 0.8;
            case "medium" -> sizeSurcharge = 1;
            case "large" -> sizeSurcharge = 1.2;
            default -> {
                System.out.println("Error");
                return 0;
            }
        }

        return basePrice
            * sizeSurcharge
            * 1.19;
    }

    public String getName() {
        return name;
    }

}
